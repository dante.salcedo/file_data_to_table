class ChangeAndAddColumnToPeople < ActiveRecord::Migration[6.0]
  def change
    rename_column :people, :name, :first_name
    add_column :people, :last_name,:string
  end
end
