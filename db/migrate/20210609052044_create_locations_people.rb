class CreateLocationsPeople < ActiveRecord::Migration[6.0]
  def change
    create_table :locations_people,  id: false do |t|
        t.belongs_to :location, index: true
        t.belongs_to :person, index: true
    end
  end
end
