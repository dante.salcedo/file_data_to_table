class CreateAffiliationsPeople < ActiveRecord::Migration[6.0]
  def change
    create_table :affiliations_people, id: false do |t|
      t.belongs_to :affiliation, index: true
      t.belongs_to :person, index: true
    end
  end
end
