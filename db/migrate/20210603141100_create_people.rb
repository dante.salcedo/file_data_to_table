class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
      t.string :name
      t.integer :species
      t.integer :gender
      t.string :weapon
      t.string :vehicle

      t.timestamps
    end
  end
end
