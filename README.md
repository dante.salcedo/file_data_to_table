# README

* Online production version: [Click Here!](http://ec2-13-210-66-57.ap-southeast-2.compute.amazonaws.com:2222)

* Description
```
-Head to: problem_description.md
```

* Ruby version
```
Ruby 2.7.1
Rails >= 6.0.3
```

* System dependencies
```
yarn >= 1.22.10
```

* Configuration
```
$cd file_data_to_table/
$bundle install
$yarn install
$export RAILS_ENV=development
```

* Database creation
```
$cd file_data_to_table/
$rails db:create
$rails db:migrate
```

* Run
```
$cd file_data_to_table/
$rails s
head to localhost:3000 in the browser 
```

* How to run the test suite
```
$cd file_data_to_table/
$rspec -fdoc

F: Uploading file and show its data
  upload a csv file, save data in DB and show Data in table

Affiliation
  Model validations
    validates valid affiliation
    validates empty name
    validates no Title Case name to Title Case
    validates uniqueness name
  Relation validations
    model relation with person

DataFile
  Fields validation
    validates one valid data_file
    validates presence of attrs
    validates type of attr data_file
    validates file extension
    validates file columns
  Processing file
    validates data persisted
    validates relations in data persisted
    validates amount of relations when same data is persisted twice

Location
  Model validations
    validates valid location
    validates empty name
    validates no Title Case name to Title Case
    validates uniqueness name
  Relation validations
    model relation with person

Person
  Fields validation
    validates one valid person
    validates gender enum values
    validates species enum values
    validates name presence
    validates p1 and p2 with same first/last name
    validates p1 and p2 with same first_name, different last_name
    validates p1 and p2 with same first_name, no last_name
    validates p1 and p2 with same first_name, p1 with last_name, p2 no last_name
  Relations validation
    validates relations with Affiliation
    validates relations with Location
    validates relations with Location and Affiliation

/data_files
  GET
    GET / and renders a successful response
    GET /data_files/new and renders a successful response
    GET /data_files/show and renders a successful response
  POST
    POST /data_files/upload and renders a successful responde

Finished in 1.77 seconds (files took 1.98 seconds to load)
34 examples, 0 failures
```



