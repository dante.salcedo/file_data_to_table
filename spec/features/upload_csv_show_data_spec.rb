require 'rails_helper'

RSpec.describe "F: Uploading file and show its data", type: :feature do
  before do
    @LOCATIONS_IN_CSV = 12
    @AFFILIATIONS_IN_CSV = 8
    @PEOPLE_IN_CSV = 16
  end

  it "upload a csv file, save data in DB and show Data in table" do
    visit data_files_new_path #visit app/views/data_files/new.html.rb
    attach_file('file', 'data_files/data.csv') #set the data file csv
    expect{
      click_on 'Submit' #upload file, should persist data
    }.to change(Person, :count).from(0).to(@PEOPLE_IN_CSV)
                 .and change(Location, :count).from(0).to(@LOCATIONS_IN_CSV)
                 .and change(Affiliation, :count).from(0).to(@AFFILIATIONS_IN_CSV)

    #data_files_controller#upload should render :new
    expect(page).to have_content('Upload CSV File')
    expect(page).to have_content(I18n.t('messages.notices.valid_file')) #successful msg
    expect(page).to have_content(I18n.t('messages.notices.data_saved')) #successful msg

    #show data and table validation
    click_link 'Show Data'
    expect(page).to have_css("h1", :text => "Show Data")
    expect(page).to have_css("table", :count => 1)
    expect(page).to have_css("tr", :count => @PEOPLE_IN_CSV+1)
    expect(page).to have_link("Home",:href=>root_path)
  end
end