require 'rails_helper'

RSpec.describe "/data_files", type: :request do
  context "GET" do
    it "GET / and renders a successful response" do
      get root_url
      expect(response).to be_successful
    end
    it "GET /data_files/new and renders a successful response" do
      get data_files_new_url
      expect(response).to be_successful
    end
    it "GET /data_files/show and renders a successful response" do
      get data_files_show_url
      expect(response).to be_successful
    end
  end

  context "POST" do
    it "POST /data_files/upload and renders a successful responde" do
      post data_files_upload_url
      expect(response).to be_successful
    end
  end
end

