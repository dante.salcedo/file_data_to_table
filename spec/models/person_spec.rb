require 'rails_helper'

RSpec.describe Person, type: :model do
  before do
    @valid_person = Person.new(first_name:"p1",species:1,gender:2,weapon:"gun",vehicle:"car")
    @invalid_person = Person.new
    @valid_person_rel = Person.new(first_name:"p2",species:1,gender:2,weapon:"gun",vehicle:"car")
    @aff1 = Affiliation.new(name:"aff1_p2")
    @aff2 = Affiliation.new(name:"aff2_p2")
    @aff3 = Affiliation.new(name:"aff3_p2")
    @loc1 = Location.new(name:"loc_p2")
    @loc2 = Location.new(name:"loc_p2")
    @loc3 = Location.new(name:"loc3_p2")
  end

  context "Fields validation" do
    it "validates one valid person" do
      @valid_person.valid?
      expect(@valid_person.errors).to be_empty
      expect{@valid_person.save}.to change(Person, :count).by(1)
    end
    it "validates gender enum values" do
      expect(@valid_person.male?).to eq(false)
      expect(@valid_person.other?).to eq(true)
      expect {@valid_person.gender = 20}.to raise_error(ArgumentError)
      expect {@valid_person.gender = "A"}.to raise_error(ArgumentError)
    end
    it "validates species enum values" do
      expect(@valid_person.astromech_droid?).to eq(false)
      expect(@valid_person.wookie?).to eq(true)
      expect { @valid_person.species = 20 }.to raise_error(ArgumentError)
      expect { @valid_person.species = "A" }.to raise_error(ArgumentError)
      @valid_person.species = ""
      expect(@valid_person.astromech_droid?).to eq(false)
      @valid_person.valid?
      expect(@valid_person.errors).to be_empty #species can be blank
    end
    it "validates name presence" do
      @invalid_person.valid?
      expect(@invalid_person.errors[:first_name]).to eq([I18n.t('errors.messages.blank')])
    end
    it "validates p1 and p2 with same first/last name" do
      #should save p1.
      p1 = Person.new(first_name:"name_a",last_name:"name_b")
      p1.valid?
      expect(p1.errors).to be_empty
      expect{p1.save}.to change(Person, :count).by(1)
      #should not save p2
      p2 = Person.new(first_name:"name_a",last_name:"name_b")
      p2.valid?
      expect(p2.errors).not_to be_empty
      expect(p2.errors[:first_name]).to eq([I18n.t('errors.messages.taken')])
      expect{p2.save}.to change(Person, :count).by(0)
    end
    it "validates p1 and p2 with same first_name, different last_name" do
      #Shoul save p1
      p1 = Person.new(first_name:"name_a",last_name:"name_b")
      p1.valid?
      expect(p1.errors).to be_empty
      expect{p1.save}.to change(Person, :count).by(1)
      #Shoul save p2
      p2 = Person.new(first_name:"name_a",last_name:"name_different")
      p2.valid?
      expect(p2.errors).to be_empty
      expect{p2.save}.to change(Person, :count).by(1)
    end
    it "validates p1 and p2 with same first_name, no last_name" do
      #should save p1.
      p1 = Person.new(first_name:"name_a")
      p1.valid?
      expect(p1.errors).to be_empty
      expect{p1.save}.to change(Person, :count).by(1)
      #should not save p2
      p2 = Person.new(first_name:"name_a")
      p2.valid?
      expect(p2.errors).not_to be_empty
      expect(p2.errors[:first_name]).to eq([I18n.t('errors.messages.taken')])
      expect{p2.save}.to change(Person, :count).by(0)
    end
    it "validates p1 and p2 with same first_name, p1 with last_name, p2 no last_name" do
      #should save p1.
      p1 = Person.new(first_name:"name_a",last_name:"name_b")
      p1.valid?
      expect(p1.errors).to be_empty
      expect{p1.save}.to change(Person, :count).by(1)
      #should save p2
      p2 = Person.new(first_name:"name_a")
      p2.valid?
      expect(p2.errors).to be_empty
      expect{p2.save}.to change(Person, :count).by(1)
    end
  end

  context "Relations validation" do
    it "validates relations with Affiliation" do
      @valid_person_rel.valid?
      expect(@valid_person_rel.errors).to be_empty
      @valid_person_rel.affiliations = [@aff1,@aff2] #should add both
      expect(@valid_person_rel.affiliations).not_to be_empty
      expect(@valid_person_rel.affiliations.length).to eq(2)
      expect(@valid_person_rel.affiliations.first.is_a?(Affiliation)).to eq(true)
      expect(@valid_person_rel.affiliations.last.name).to eq(@aff2.name)
      expect {@valid_person_rel.affiliations = [1,2]}.to raise_error(ActiveRecord::AssociationTypeMismatch)

      @valid_person_rel.affiliations.append(@aff1) #shouldn't add
      expect(@valid_person_rel.affiliations.length).to eq(2)
      @valid_person_rel.affiliations.append(@aff2) #shouldn't add
      expect(@valid_person_rel.affiliations.length).to eq(2)
      #replace all affiliations. Add only one @aff1
      @valid_person_rel.affiliations = [@aff1,@aff2,@aff1,@aff3]
      expect(@valid_person_rel.affiliations.length).to eq(3)
      expect(@valid_person_rel.affiliations.last.id).to eq(@aff3.id)
      @valid_person_rel.valid?
      expect(@valid_person_rel.errors).to be_empty
    end

    it "validates relations with Location" do
      @valid_person_rel.valid?
      expect(@valid_person_rel.errors).to be_empty
      @valid_person_rel.locations = [@loc1,@loc2] #should add both
      expect(@valid_person_rel.locations).not_to be_empty
      expect(@valid_person_rel.locations.length).to eq(2)
      expect(@valid_person_rel.locations.first.is_a?(Location)).to eq(true)
      expect(@valid_person_rel.locations.last.name).to eq(@loc2.name)
      expect {@valid_person_rel.locations = [1,2]}.to raise_error(ActiveRecord::AssociationTypeMismatch)

      @valid_person_rel.locations.append(@loc1) #shouldn't add
      expect(@valid_person_rel.locations.length).to eq(2)
      @valid_person_rel.locations.append(@loc2) #shouldn't add
      expect(@valid_person_rel.locations.length).to eq(2)
      #replace all locations. Add only one @loc2
      @valid_person_rel.locations = [@loc2,@loc1,@loc2,@loc3]
      expect(@valid_person_rel.locations.length).to eq(3)
      expect(@valid_person_rel.locations.last.id).to eq(@loc3.id)
      @valid_person_rel.valid?
      expect(@valid_person_rel.errors).to be_empty
    end

    it "validates relations with Location and Affiliation" do
      @valid_person_rel.valid?
      expect(@valid_person_rel.errors).to be_empty
      @valid_person_rel.affiliations = [@aff1,@aff2,@aff1,@aff3] #should add @aff1(once),@aff2,@aff3
      expect(@valid_person_rel.affiliations.length).to eq(3)
      @valid_person_rel.locations = [@loc2,@loc1,@loc2,@loc3] #should add @loc1,@loc2(once),@loc3
      expect(@valid_person_rel.locations.length).to eq(3)
    end
  end
end
