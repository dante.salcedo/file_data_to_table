require 'rails_helper'

RSpec.describe DataFile, type: :model do
  #{\"file\"=>#<Rack::Test::UploadedFile:0x00007ffa67bacd68 @original_filename=\"data.csv\", @tempfile=#<Tempfile:/var/../dl0d4.csv>, @content_type=nil>}
  let(:csv_file) { fixture_file_upload('data_files/data.csv') }
  let(:md_file) { fixture_file_upload('README.md')  }
  let(:wrong_csv_file) { fixture_file_upload('data_files/data_wrong_columns.csv') }
  before do
    @valid_data_file = DataFile.new(csv_file)
    @LOCATIONS_IN_CSV = 12
    @AFFILIATIONS_IN_CSV = 8
    @PEOPLE_IN_CSV = 16
    @REL_AFFILIATION_PEOPLE_IN_CSV = 21
    @REL_LOCATION_PEOPLE_IN_CSV = 17
    @invalid_file_extension = DataFile.new(md_file)
    @invalid_file_columns = DataFile.new(wrong_csv_file)
  end

  context "Fields validation" do
    it "validates one valid data_file" do
      @valid_data_file.valid?
      expect(@valid_data_file.errors).to be_empty
    end

    it "validates presence of attrs" do
      @valid_data_file.data_file = nil
      @valid_data_file.valid?
      expect(@valid_data_file.errors).not_to be_empty
      expect(@valid_data_file.errors[:data_file]).to eq([I18n.t('errors.messages.blank')])
    end

    it "validates type of attr data_file" do
      @valid_data_file.data_file = "string not File"
      @valid_data_file.valid?
      expect(@valid_data_file.errors).not_to be_empty
      expect(@valid_data_file.errors[:data_file]).to eq([I18n.t('messages.errors.file_data.wrong_type')])
    end

    it "validates file extension" do
      @invalid_file_extension.valid?
      expect(@invalid_file_extension.errors).not_to be_empty
      expect(@invalid_file_extension.errors[:data_file]).to eq([I18n.t('messages.errors.file_data.wrong_file_ext')])
    end

    it "validates file columns" do
      @invalid_file_columns.valid?
      expect(@invalid_file_columns.errors).not_to be_empty
      expect(@invalid_file_columns.errors[:data_file]).to eq([I18n.t('messages.errors.file_data.wrong_headers')])
    end
  end

  context "Processing file" do
    it "validates data persisted" do
      @valid_data_file.valid?
      expect(@valid_data_file.errors).to be_empty
      expect(Person.all.length).to eq(0)
      expect(Location.all.length).to eq(0)
      expect(Affiliation.all.length).to eq(0)

      @valid_data_file.persist_data

      expect(Location.all.length).not_to eq(0)
      expect(Location.all.length).to eq(@LOCATIONS_IN_CSV)
      expect(Affiliation.all.length).not_to eq(0)
      expect(Affiliation.all.length).to eq(@AFFILIATIONS_IN_CSV)
      expect(Person.all.length).not_to eq(0)
      expect(Person.all.length).to eq(@PEOPLE_IN_CSV)
    end

    it "validates relations in data persisted" do
      @valid_data_file.valid?
      expect(@valid_data_file.errors).to be_empty

      if @valid_data_file.persist_data
        expect(Location.all.length).to eq(@LOCATIONS_IN_CSV)
        expect(Affiliation.all.length).to eq(@AFFILIATIONS_IN_CSV)
        expect(Person.all.length).to eq(@PEOPLE_IN_CSV)

        p_array = Person.all.where(first_name: "Princess",last_name:"Leia")
        expect(p_array.length).to eq(1) #1 Princess Leia
        expect(p_array[0].affiliations.length).to eq(2)
        expect(p_array[0].locations.length).to eq(1)
        expect(p_array[0].locations[0].name).to eq("Alderaan")

        p_array = Person.all.where(first_name: "Darth",last_name:"Vadar")
        expect(p_array.length).to eq(1) #1 Darth Vadar
        person = p_array[0]
        expect(person.locations.length).to eq(2)
        expect(person.locations[0].name).to eq("Death Star")
        expect(person.locations[1].name).to eq("Tatooine")
        expect(person.affiliations.length).to eq(2)
        expect(person.affiliations[0].name).to eq("Sith")
        expect(person.affiliations[1].name).to eq("The Resistance")
      end
    end

    it "validates amount of relations when same data is persisted twice" do
      @valid_data_file.valid?
      expect(@valid_data_file.errors).to be_empty
      if @valid_data_file.persist_data
        relations_aff_per = 0
        relations_loc_per = 0
        Person.all.each do |p|
          relations_aff_per = relations_aff_per + p.affiliations.length
          relations_loc_per = relations_loc_per + p.locations.length
        end
        expect(relations_aff_per).to eq(@REL_AFFILIATION_PEOPLE_IN_CSV)
        expect(relations_loc_per).to eq(@REL_LOCATION_PEOPLE_IN_CSV)

        @valid_data_file.persist_data #should not insert more relations
        relations_aff_per = 0
        relations_loc_per = 0
        Person.all.each do |p|
          relations_aff_per = relations_aff_per + p.affiliations.length
          relations_loc_per = relations_loc_per + p.locations.length
        end
        expect(relations_aff_per).to eq(@REL_AFFILIATION_PEOPLE_IN_CSV)
        expect(relations_loc_per).to eq(@REL_LOCATION_PEOPLE_IN_CSV)
      end #else => will raise error
    end
  end
end