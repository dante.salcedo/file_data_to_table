require 'rails_helper'

RSpec.describe Location, type: :model do
  before do
    @valid_location = Location.new(name: 'LocaA')
    @location_empty_name = Location.new()
    @location_not_tc_name = Location.new(name: "location no title case")
    @per1 = Person.new(first_name:"p1",species:1,gender:2,weapon:"gun",vehicle:"car")
    @per2 = Person.new(first_name:"p2",species:3,gender:1,weapon:"gun",vehicle:"car")
    @per3 = Person.new(first_name:"p3",species:4,gender:0,weapon:"gun",vehicle:"car")
  end

  context "Model validations" do
    it "validates valid location" do
      @valid_location.valid?
      expect(@valid_location.errors[:name]).to eq([])
      expect{@valid_location.save}.to change(Location, :count).by(1)
    end
    it "validates empty name" do
      @location_empty_name.valid?
      expect(@location_empty_name.errors[:name]).not_to be_empty
      expect(@location_empty_name.errors[:name]).to eq([I18n.t('errors.messages.blank')])
    end
    it "validates no Title Case name to Title Case" do
      @location_not_tc_name.valid?
      expect(@location_not_tc_name.errors[:name]).to be_empty
      #not errors in :name. before to validate the model execute name_to_title_case
    end
    it "validates uniqueness name" do
      @valid_location.valid?
      expect{@valid_location.save}.to change(Location, :count).by(1)
      same_name = @valid_location.name
      l_new = Location.new(name: same_name)
      l_new.valid?
      expect(l_new.errors[:name]).not_to be_empty
      expect(l_new.errors[:name]).to eq([I18n.t('errors.messages.taken')])
    end
  end

  context "Relation validations" do
    it "model relation with person" do
      @valid_location.valid?
      expect(@valid_location.errors).to be_empty
      @valid_location.people = [@per1,@per2] #should add both
      expect(@valid_location.people).not_to be_empty
      expect(@valid_location.people.length).to eq(2)
      expect(@valid_location.people.first.is_a?(Person)).to eq(true)
      expect(@valid_location.people.last.first_name).to eq(@per2.first_name)
      expect{@valid_location.people = [1,2]}.to raise_error(ActiveRecord::AssociationTypeMismatch)

      @valid_location.people.append(@per1) #shouldn't add
      expect(@valid_location.people.length).to eq(2)
      @valid_location.people.append(@per2) #shouldn't add
      expect(@valid_location.people.length).to eq(2)
      @valid_location.people = [@per1,@per2,@per1,@per3] #should add only @aff3
      expect(@valid_location.people.length).to eq(3)
      expect(@valid_location.people.last.id).to eq(@per3.id)
      @valid_location.valid?
      expect(@valid_location.errors).to be_empty
    end
  end
end