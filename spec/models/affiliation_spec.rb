require 'rails_helper'

RSpec.describe Affiliation, type: :model do
  before do
    @valid_affiliation = Affiliation.new(name: "AffA")
    @affiliation_empty_name = Affiliation.new()
    @affiliation_not_tc_name = Affiliation.new(name: "affiliation no title case")
    @per1 = Person.new(first_name:"p1",species:1,gender:2,weapon:"gun",vehicle:"car")
    @per2 = Person.new(first_name:"p2",species:3,gender:1,weapon:"gun",vehicle:"car")
    @per3 = Person.new(first_name:"p3",species:4,gender:0,weapon:"gun",vehicle:"car")
  end

  context "Model validations" do
    it "validates valid affiliation" do
      @valid_affiliation.valid?
      expect(@valid_affiliation.errors[:name]).to eq([])
      expect{@valid_affiliation.save}.to change(Affiliation, :count).by(1)
    end
    it "validates empty name" do
      @affiliation_empty_name.valid?
      expect(@affiliation_empty_name.errors[:name]).not_to be_empty
      expect(@affiliation_empty_name.errors[:name]).to eq([I18n.t('errors.messages.blank')])
    end
    it "validates no Title Case name to Title Case" do
      @affiliation_not_tc_name.valid?
      expect(@affiliation_not_tc_name.errors[:name]).to be_empty
    end
    it "validates uniqueness name" do
      @valid_affiliation.valid?
      expect{@valid_affiliation.save}.to change(Affiliation, :count).by(1)
      same_name = @valid_affiliation.name
      l_new = Affiliation.new(name: same_name)
      l_new.valid?
      expect(l_new.errors[:name]).not_to be_empty
      expect(l_new.errors[:name]).to eq([I18n.t('errors.messages.taken')])
    end
  end

  context "Relation validations" do
    it "model relation with person" do
      @valid_affiliation.valid?
      expect(@valid_affiliation.errors).to be_empty
      @valid_affiliation.people = [@per1,@per2] #should add both
      expect(@valid_affiliation.people).not_to be_empty
      expect(@valid_affiliation.people.length).to eq(2)
      expect(@valid_affiliation.people.first.is_a?(Person)).to eq(true)
      expect(@valid_affiliation.people.last.first_name).to eq(@per2.first_name)
      expect{@valid_affiliation.people = [1,2]}.to raise_error(ActiveRecord::AssociationTypeMismatch)

      @valid_affiliation.people.append(@per1) #shouldn't add
      expect(@valid_affiliation.people.length).to eq(2)
      @valid_affiliation.people.append(@per2) #shouldn't add
      expect(@valid_affiliation.people.length).to eq(2)
      @valid_affiliation.people = [@per1,@per2,@per1,@per3] #should add only @aff3
      expect(@valid_affiliation.people.length).to eq(3)
      expect(@valid_affiliation.people.last.id).to eq(@per3.id)
      @valid_affiliation.valid?
      expect(@valid_affiliation.errors).to be_empty
    end
  end
end
