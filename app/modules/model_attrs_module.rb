module ModelAttrsModule
  # Convert an Attribute string in a Title Case string
  #in Model add
  #-include ModelAttrsModule
  #-before_validation {string_to_title_case("name")}
  # @param: [String] attr_name. Model must have this attribute name
  def attr_string_to_title_case(attr_name)
    attr_value = self[attr_name]
    if !attr_value.is_a?(NilClass) && !attr_value.empty?
      self[attr_name] = self[attr_name].split(" ").collect{|word| word[0] = word[0].upcase; word}.join(" ")
    end
  end
end