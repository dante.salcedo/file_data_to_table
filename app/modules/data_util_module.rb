module DataUtilModule

  #Remove anything not contained in the regex
  #regex contains dash,simple_quote,space
  #Numbers values are converted to empty
  # @param [String] s.
  # @return [String] s. Cleaned
  def self.clean_weapon_vehicle(s)
    if !s.is_a?(NilClass) && !s.empty?
      if /^-?\d*\.{0,1}\d+$/.match(s) #Is numeric value
        s = ""
      else
        s = s.gsub(/[^0-9A-Za-z\-' ]/, '')
      end
    end
    return s
  end

  #Convert a string to valid enum key
  # @use: DataUtilModule::string_to_enum_key(string)
  # @example: "Protocol Droid" => "protocol_droid"
  # @param: [String] s. string to be converted
  # @return: [String] s. Enum key for Person.species["protocol_droid"]. Empty if no conversion
  def self.string_to_enum_key(s)
    if !s.is_a?(NilClass) && !s.empty?
      return s.downcase.gsub(" ","_")
    else
      return ""
    end
  end

  # Convert a string to valid enum gender key
  # Person.genders: [:male, :female, :other]
  # @example: "m" => "male"
  # @example: "Male" => "male"
  # @param: [String] g. gender string to be converted
  # @return: [String] g. Enum key for Person.gender. Empty if no conversion
  def self.gender_to_enum_key(g)
    if !g.is_a?(NilClass) && !g.empty?
      g = g.downcase
      if g == "m"
        g = "male"
      end
      if g == "f"
        g = "female"
      end
      if g == "o"
        g = "other"
      end
      return g
    else
      return ""
    end
  end

  #Convert a string in a Title Case string
  # First char in every word is upcase
  # @use: DataUtilModule::string_to_title_case(string)
  # @example: "padme amidala" => "Padme Amidala"
  # @param: [String] s. string to be converted
  # @return: [String] s. Title case string
  def self.string_to_title_case(s)
    if !s.is_a?(NilClass) && !s.empty?
      return s.split(" ").collect{|word| word[0] = word[0].upcase; word}.join(" ")
    end
  end
end