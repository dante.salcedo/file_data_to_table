module DataFilesModule
  attr_accessor :data_file #File
  attr_accessor :columns_array #Array<String>

  class ColumnPosition
    FULL_NAME=0
    LOCATIONS=1
    SPECIES=2
    GENDER=3
    AFFILIATIONS=4
    WEAPON=5
    VEHICLE=6
  end

  # @param: [File] file.
  def initialize(file)
    @data_file = file
    #use to validate file columns
    @columns_array = ["Name","Location","Species","Gender","Affiliations","Weapon","Vehicle"]
  end

  # Read the csv file and persist only NEW Data.
  # Save People,Locations and Affiliations in the DB
  # Add the respective locations and affiliations to each person.
  # When thisObject.valid? is true. Call thisObject.persist_data
  # @return [Boolean]. True if all data was persisted, False in otherwise.
  # If it fails, in the next execution will persist only new Data.
  def persist_data
    begin
      require 'csv'
      i = 0 #0 are headers. Already validated.
      actual_row = []
      # CSV.foreach(@data_file["file"]) do |row| #Process 1 row each cycle
      CSV.foreach(@data_file) do |row| #Process 1 row each cycle
        if i!=0
          actual_row = row
          affiliations = row[ColumnPosition::AFFILIATIONS] #string comma separated
          names_array = row[ColumnPosition::FULL_NAME].split(" ") #string " " separated

          #Skipped if name or affiliation is nil or empty.
          if (!affiliations.is_a?(NilClass) && !names_array.is_a?(NilClass) && !affiliations.empty? && names_array.length>0)
            #Create Locations
            locations = row[ColumnPosition::LOCATIONS] #string comma separated
            locations_objects = create_locations(locations)

            #Create Affiliations
            affiliations_objects = create_affiliations(affiliations)

            #Create People
            full_name = row[ColumnPosition::FULL_NAME]
            species =  DataUtilModule::string_to_enum_key(row[ColumnPosition::SPECIES])
            species = Person.species[species] #if species == "". species will be empty
            gender =  DataUtilModule::gender_to_enum_key(row[ColumnPosition::GENDER])
            gender = Person.genders[gender] #if gender == "". gender will be empty
            weapon =  DataUtilModule::clean_weapon_vehicle(row[ColumnPosition::WEAPON])
            vehicle = DataUtilModule::clean_weapon_vehicle(row[ColumnPosition::VEHICLE])
            create_person(full_name,species,gender,weapon,vehicle,locations_objects,affiliations_objects)
          end
        end
        i=i+1
      end
      return true
    rescue => error
      Rails.logger.debug error.inspect
      Rails.logger.debug "Row: #{actual_row}"
      return false
    end
  end

  private

  # Insert only the new locations in the "locations" table.
  # @param: [String] locations. Comma separated, i.e: "Death Star, Tatooine"
  # @return: [Array<Location>] locations_objects. List of objects type Location
  def create_locations(locations)
    locations_objects = []
    loc_array=locations.split(",")
    loc_array.each do |l|
      l = remove_first_withe_space(l) #["Death Star"," Tatooine"]
      l =  DataUtilModule::string_to_title_case(l)
      loc_obj = Location.find_or_create_by(name: l)
      locations_objects.push(loc_obj)
    end
    return locations_objects
  end

  # Insert only the new affiliations in the "affiliations" table.
  # @param: [String] affiliations. Comma separated, i.e: "Rebel Alliance, Jedi Order"
  # @return: [Array<Affiliation>] affiliations_objects. List of objects type Affiliation
  def create_affiliations(affiliations)
    affiliations_objects = []
    aff_array=affiliations.split(",")
    aff_array.each do |a|
      a = remove_first_withe_space(a)
      a =  DataUtilModule::string_to_title_case(a)
      aff_obj= Affiliation.find_or_create_by(name: a)
      affiliations_objects.push(aff_obj)
    end
    return affiliations_objects
  end


  # Create the person record in the db
  # Add locations and affiliation to this person
  # @param [String] full_name. i.e: "jabba the Hutt"
  # @param [Integer] species. Int in enum Person.species
  # @param [Integer] gender. Int in enum Person.genders
  # @param [String] weapon
  # @param [String] vehicle
  # @param [Array<Location>] locations
  # @param [Array<Affiliation>] affiliations
  def create_person(full_name, species, gender, weapon, vehicle, locations, affiliations)
    last_name=""
    first_name=""
    i = 0
    names_array=full_name.split(" ")
    names_array.each do |name|
      i == 0 ? first_name = name : last_name = last_name+" "+name
      i=i+1
    end
    last_name = DataUtilModule::string_to_title_case(remove_first_withe_space(last_name))
    first_name = DataUtilModule::string_to_title_case(first_name)
    person = Person.create_with(species:species,gender:gender,weapon:weapon,vehicle:vehicle).find_or_create_by(first_name:first_name,last_name:last_name)

    #If the person already exists, only add new affiliations/locations to it.
    affiliations.each do |a|
      unless person.affiliations.any? {|person_l| person_l.name == a.name }
        person.affiliations.append(a)
      end
    end
    locations.each do |l|
      unless person.locations.any? {|person_l| person_l.name == l.name }
        person.locations.append(l)
      end
    end
  end

  # Some values have an empty space in the [0] position.
  # @param: [String] s. i.e: " Tatooine"
  # @return: [String] s. "Tatooine"
  def remove_first_withe_space(s)
    s.start_with?(" ") ? s[0] = '' : s
    return s
  end

end