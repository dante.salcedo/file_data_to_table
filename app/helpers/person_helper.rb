module PersonHelper

  # @param [Person] person
  # @return [String]. First name + last name if exists
  def format_name(person)
    last_name = person.last_name.is_a?(NilClass)? "" : person.last_name
    return person.first_name+" "+last_name
  end

  # @param [Person] person
  # @return [String]. All person locations. String comma separated
  def format_locations(person)
    all_locations = ""
    person.locations.each { |loc| all_locations+=", "+loc.name }
    all_locations[0]="" #remove the first ,
    all_locations[0]="" #remove the first space ' '
    return all_locations
  end

  # @param [Person] person
  # @return [String]. All person affiliations. String comma separated
  def format_affiliations(person)
    all_affiliations = ""
    person.affiliations.each { |aff| all_affiliations+=", "+aff.name }
    all_affiliations[0]="" #remove the first ,
    all_affiliations[0]="" #remove the first space ' '
    return all_affiliations
  end


end