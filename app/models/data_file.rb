class DataFile#PORO
  # ------modules-----
  include ActiveModel::Validations
  include DataFilesModule

  # ------validations------
  validates :data_file, presence: true
  #test=>Class=> Rack::Test::UploadedFile
  #{\"file\"=>#<Rack::Test::UploadedFile:0x00007ffa67bacd68 @original_filename=\"data.csv\", @tempfile=#<Tempfile:/var/../dl0d4.csv>, @content_type=nil>}
  #browser GUI=> Class =>ActionDispatch::Http::UploadedFile
  # "file"=>
  #   #<ActionDispatch::Http::UploadedFile:0x00007fe0ea31b618
  #    @content_type="text/csv",
  #    @headers="Content-Disposition: form-data; name=\"file\"; filename=\"data.csv\"\r\n" + "Content-Type: text/csv\r\n",
  #    @original_filename="data.csv",
  #    @tempfile=#<File:/var/../srmt4c.csv>>,
  validates_with AttributeFileValidator, {attr_name:"data_file"}
end