class Person < ApplicationRecord
  #----modules------
  include ModelAttrsModule

  #----attrs------
  enum gender: [:male, :female, :other]
  enum species: [:human, :wookie, :hutt,:gungan,:astromech_droid,:protocol_droid,:unknown]

  #----relations------
  # By default HABTM relations don't validate duplicates
  # in the join tables (affiliations_people/locations_people).
  # Doesn't work adding index in the migration file. i.e:
  # add_index :affiliations_people, [:affiliation_id, :person_id], unique: true
  # Following lines work at Object level, i.e:
  # i.e-1 Will not insert @aff1
  # person.affiliations = [@aff1,@aff2]
  # person.affiliations.append(@aff1)
  # i.e-2 Will insert @aff
  # person.affiliations = [@aff1(name="Sith"),@aff2]
  # @aff = find_by(name:"Sith")
  # person.affiliations.append(@aff) #will insert @aff
  has_and_belongs_to_many :affiliations, -> { distinct } do
    def << (value)
      super value rescue ActiveRecord::RecordNotUnique
    end
  end
  has_and_belongs_to_many :locations, -> { distinct } do
    def << (value)
      super value rescue ActiveRecord::RecordNotUnique
    end
  end

  #----validations------
  validates :first_name, presence: true,  uniqueness: { scope: :last_name }
  validates_with AttributeTitlecaseValidator, {attr_name:"first_name"}
  validates_with AttributeTitlecaseValidator, {attr_name:"last_name"}
  before_validation {attr_string_to_title_case("first_name")}
  before_validation {attr_string_to_title_case("last_name")}
end
