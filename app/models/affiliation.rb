class Affiliation < ApplicationRecord
  #----modules------
  include ModelAttrsModule

  #----relations------
  has_and_belongs_to_many :people, -> { distinct } do
    def << (value)
      super value rescue ActiveRecord::RecordNotUnique
    end
  end

  #----validations------
  validates :name, presence: true, uniqueness: true
  validates_with AttributeTitlecaseValidator, {attr_name:"name"}
  before_validation {attr_string_to_title_case("name")}
end
