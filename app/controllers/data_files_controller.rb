class DataFilesController < ApplicationController
  #get new
  def new

  end

  #get show
  def show
    @people=Person.all
  end

  #post upload
  def upload
    csv_file = params[:file]
    data_file_obj = DataFile.new(csv_file)
    if !data_file_obj.valid?
      flash.now[:error] = data_file_obj.errors.full_messages
    else
      if data_file_obj.persist_data
        flash.now[:alert] = I18n.t('messages.notices.valid_file')
        flash.now[:notice] = I18n.t('messages.notices.data_saved')
      else
        flash.now[:alert] = I18n.t('messages.notices.valid_file')
        flash.now[:error] = I18n.t('messages.errors.saving_data')
      end
    end
    render :new
  end
end