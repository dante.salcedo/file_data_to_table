class AttributeFileValidator < ActiveModel::Validator

  #call in model:
  #validates_with AttributeFileValidator, {attr_name:"data_file"}
  # @param [ActiveModel] record.
  # @param [String] :attr_name. Should be UploadedFile type
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if is_file(record,attr_name,attr_value)
      if is_csv(record,attr_name,attr_value)
        has_valid_columns(record,attr_name,attr_value)
      end
    end
  end

  private

  #Check if the Attribute is a valid File Type.
  # @param [ActiveModel] record.
  # @param [String] attr_name. attr's name.
  # @param [?] attr_value. value of attr_name.
  # @return [Boolean]: true. If the attr's type is: Rack::Test::UploadedFile or ActionDispatch::Http::UploadedFile?
  #false in otherwise.
  def is_file(record,attr_name,attr_value)
    # if !attr_value.is_a?(NilClass) && !attr_value.empty?
    if !attr_value.is_a?(NilClass)
      if !attr_value.is_a?(Rack::Test::UploadedFile) && !attr_value.is_a?(ActionDispatch::Http::UploadedFile)
        if attr_value.is_a?(Hash)
          is_file = false
          attr_value.values.each  do |v|
            if v.is_a?(Rack::Test::UploadedFile) || v.is_a?(ActionDispatch::Http::UploadedFile)
              is_file = true
              break
            end
          end
          if is_file
            return true
          else
            record.errors.add(attr_name,I18n.t('messages.errors.file_data.wrong_type') )
            return false
          end
        else
          record.errors.add(attr_name, I18n.t('messages.errors.file_data.wrong_type'))
          return false
        end
      else
        return true
      end
    end
    #if attr_value == (nil or empty). it's validated with presence:true
  end
  #Check if the File ends with .csv extension.
  # @param [ActiveModel] record.
  # @param [String] attr_name. attr's name.
  # @param [?] attr_value. value of attr_name.
  # @return [Boolean]: true. If the original file name ends with .csv
  # false in otherwise.
  def is_csv(record,attr_name,attr_value)
    if !attr_value.is_a?(Rack::Test::UploadedFile) && !attr_value.is_a?(ActionDispatch::Http::UploadedFile)
      attr_value.values.each  do |v|
        if v.is_a?(Rack::Test::UploadedFile) || v.is_a?(ActionDispatch::Http::UploadedFile)
          attr_value = v
          break
        end
      end
    end
    if attr_value.original_filename.end_with?(".csv")
      return true
    else
      record.errors.add(attr_name, I18n.t('messages.errors.file_data.wrong_file_ext'))
      return false
    end
  end

  # Check if the file headers are the same with DataFilesModule.columns_array
  # @param [ActiveModel] record.
  # @param [String] attr_name. attr's name.
  # @param [Rack::Test::UploadedFile,ActionDispatch::Http::UploadedFile] attr_value. value of attr_name.
  def has_valid_columns(record,attr_name,attr_value)
    require 'csv'
    CSV.foreach(attr_value) do |row|
    # CSV.foreach(attr_value["file"]) do |row|
      total_columns = record.columns_array.length
      if total_columns == row.length
        if record.columns_array == row #Check if all elements are the same
          return true
        end
      end
      record.errors.add(attr_name,I18n.t('messages.errors.file_data.wrong_headers') )
      return false
    end
  end
end