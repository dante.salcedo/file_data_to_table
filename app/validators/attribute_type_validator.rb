class AttributeTypeValidator < ActiveModel::Validator

  #call in model:
  #validates_with AttributeTypeValidator, {attr_name:"data_file",class_type:Hash}
  # @param [ActiveModel] record.
  # @param [String] :attr_name.
  # @param [Class] :class_type. i.e: File,String,Integer
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if !attr_value.is_a?(NilClass) && !attr_value.empty?
      class_type = options[:class_type]
      if !attr_value.is_a?(class_type)
        record.errors.add(attr_name,I18n.t('messages.errors.attr_wrong_class',{attr_name:attr_name,class_type:class_type}))
      end
    end
    #if attr_value == (nil or empty). it's validated with presence:true
  end
end