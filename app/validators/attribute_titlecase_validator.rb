class AttributeTitlecaseValidator < ActiveModel::Validator

  #call in model:
  #validates_with AttributeTitlecaseValidator, {attr_name:"name"}
  # @param [ActiveModel] record.
  # @param [String] :attr_name.
  def validate(record)
    attr_name = options[:attr_name]
    attr_value = record.send(attr_name)
    if !attr_value.is_a?(NilClass) && !attr_value.empty?
      if attr_value.split.any?{|w|w[0].upcase != w[0]}
        record.errors.add(attr_name, I18n.t('messages.errors.not_title_case',attr_name: attr_name))
      end
    end
  end
end
