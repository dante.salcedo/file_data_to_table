# Problem Description
`PART 1`

As a user, I should be able to upload the sample CSV (data.csv) and import the data into a database.

IMPORTER REQUIREMENTS
```
The data needs to load into 3 tables. People, Locations and Affiliations
A Person can belong to many Locations
A Person can belong to many Affiliations
A Person without an Affiliation should be skipped
A Person should have both a first_name and last_name. All fields need to be validated except for last_name, weapon and vehicle which are optional.
Names and Locations should all be titlecased
```

`PART 2`

TABLE REQUIREMENTS
```
As a user, I should be able to view these results from the importer in a table.
As a user, I should be able to paginate through the results so that I can see a maximum of 10 results at a time.
As a user, I want to type in a search box so that I can filter the results I want to see.
As a user, I want to be able to click on a table column heading to reorder the visible results.
```