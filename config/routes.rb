Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get "/data_files/new",to: "data_files#new", as: "data_files_new"
  root "data_files#new"
  get "/data_files/show",to: "data_files#show", as: "data_files_show"
  post "/data_files/upload",to: "data_files#upload", as: "data_files_upload"

end
